require_relative "lib/engine49688/version"

Gem::Specification.new do |spec|
  spec.name        = "engine49688"
  spec.version     = Engine49688::VERSION
  spec.authors     = ["Vladimir Támara Patiño"]
  spec.email       = ["vtamara@pasosdeJesus.org"]
  spec.homepage    = "https://example.com"
  spec.summary     = "Summary of Engine49688."
  spec.description = "Description of Engine49688."
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the "allowed_push_host"
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  spec.metadata["allowed_push_host"] = "https://example.com"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://example.com"
  spec.metadata["changelog_uri"] = "https://example.com"

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]
  end

  spec.add_dependency "rails", ">= 7.0.8"
end
