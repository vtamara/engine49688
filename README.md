# Engine49688

Minimal engine to illustrate the problem https://github.com/rails/rails/issues/49688

Based on the blorgh example of https://edgeguides.rubyonrails.org/engines.html but withouth the resource `comment` (just with the resource `article`).

## Usage
See articles in your application.

## Installation
Add this line to your application's Gemfile:

```ruby
gem "engine49688"
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install engine49688
```

## How did we generate this engine?

We used OpenBSD/adJ 7.4a1 that includes ruby 3.2.2 and sqlite 3.42.0 and installed rails 7.0 globally (by running `doas gem uninstall rails; doas gem install rails --version 7.0.8`) and followed these instructions from https://edgeguides.rubyonrails.org/engines.html:

```sh
    rails plugin new engine49688 --mountable
    cd engine49688
```

We edited `engine49688.gemspec` and changed **TODO**, we made sure that following operations would be with rails 7.0 changin temporarely the line `spec.add_dependency "rails", ">= 7.1.2"` with `spec.add_dependency "rails", "~> 7.0.8"` then we run:

```sh
    bundle
    bin/rails generate scaffold article title:string text:text
    bin/rails db:migrate
```

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
